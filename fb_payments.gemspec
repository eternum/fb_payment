$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "fb_payments/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "fb_payments"
  s.version     = FbPayments::VERSION
  s.authors     = ["Jack Hanley"]
  s.email       = ["jack@cagedmedia.com"]
  s.homepage    = "http://immers.io"
  s.summary     = "Summary of FbPayments."
  s.description = "Description of FbPayments."
  s.license     = "MIT"

  s.files = `git ls-files`.split("\n")   #Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.1.8"

  s.add_development_dependency "sqlite3" 
  s.add_development_dependency "rspec"
  s.add_development_dependency "dotenv"


  s.add_dependency "devise"  
  s.add_dependency "omniauth-facebook"
  s.add_dependency "paperclip"


end
