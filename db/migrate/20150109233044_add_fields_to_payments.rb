class AddFieldsToPayments < ActiveRecord::Migration
  def change
    add_column :fb_payments_payments, :user_id, :integer
    add_column :fb_payments_payments, :fb_id, :string
    add_column :fb_payments_payments, :product, :string
    add_column :fb_payments_payments, :quantity, :integer
    add_column :fb_payments_payments, :request_id, :string
    add_column :fb_payments_payments, :country, :string
    add_column :fb_payments_payments, :tax, :string
    add_column :fb_payments_payments, :tax_country, :string
    add_column :fb_payments_payments, :payout_foreign_exchange_rate, :float
    add_column :fb_payments_payments, :test, :bool
    add_column :fb_payments_payments, :application_id, :integer
  end
end
