class CreateFbPaymentsApplications < ActiveRecord::Migration
  def change
    create_table :fb_payments_applications do |t|
      t.string :name
      t.string :namespace
      t.string :fb_id

      t.timestamps
    end
  end
end
