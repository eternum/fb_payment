class AddProductTypeToProducts < ActiveRecord::Migration
  def change
    add_column :fb_payments_products, :product_type, :integer, default: 0
  end
end
