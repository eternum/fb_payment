class AddFieldsToProducts < ActiveRecord::Migration
  def change
    add_column :fb_payments_products, :name, :string
    add_column :fb_payments_products, :description, :string
    add_column :fb_payments_products, :cost, :string
  end
end
