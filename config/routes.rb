FbPayments::Engine.routes.draw do
  get '/realtime/payments', :to =>'realtime#challenge'
  post '/realtime/payments', :to =>'realtime#payments_create'
  root "home#index"
  resources :products
  get '/products/:id/:quantity' => "products#show"
end
