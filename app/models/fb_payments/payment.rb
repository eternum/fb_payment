module FbPayments
  class Payment < ActiveRecord::Base
    belongs_to :application
    def refunded

    end
    def check_for_refunds(args)
      any_refunds=false
      test = false
      args.each do |p|
          any_refunds=true if p["type"]=="refund"

      end
      return any_refunds
    end
    def self.build_from_api(args)
      @application = FbPayments::FbApplication.find_or_initialize_by(:fb_id=>args["application"]["id"])
      @payment = FbPayments::Payment.new(
                                        :user_id,
                                        :fb_id=>args["id"],
                                        :product,
                                        :quantity,
                                        :request_id,
                                        :country,
                                        :tax,
                                        :tax_country,
                                        :payout_foreign_exchange_rate,
                                        :test
      )
      @payment.application_id = @application.id

    end

  end
end
