module FbPayments
  class Product < ActiveRecord::Base
    enum product_type: { currency: 0, package: 1 }
    has_attached_file :image, :styles => { :medium => "238x238>",
                                         :thumb => "100x100>"
                          }
  end
end
