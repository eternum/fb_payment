module FbPayments
  class Application < ActiveRecord::Base
    has_many :payments
    has_many :products
  end
end
