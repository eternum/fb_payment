require_dependency "fb_payments/application_controller"

module FbPayments
  class ProductsController < ApplicationController

    def index
      @products=Product.all
    end
    def show

      @product=Product.find(params[:id])
      @quantity = params[:quantity].to_i || 1
      respond_to do |format|
        format.html{ render layout: false}
        format.json {render :json=>@product}
      end
      # respond_to do |format|
      #   format.html {render "og_"+@product_type, :layout=>nil}
      # end
    end
  end
end
