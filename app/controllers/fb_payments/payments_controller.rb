require_dependency "fb_payments/application_controller"

module FbPayments
  class PaymentsController < ApplicationController
    def create
      @payment = FbPayments::Payment.build_from_api(
                                        params
      )
    end
  end
end
# ####
# {
#     "id": "3603105474213890",
#     "user": {
#     "name": "Daniel Schultz",
#     "id": "221159"
# },
#     "application": {
#     "name": "Friend Smash",
#     "namespace": "friendsmashsample",
#     "id": "241431489326925"
# },
#     "actions": [
#     {
#         "type": "charge",
#     "status": "completed",
#     "currency": "USD",
#     "amount": "0.99",
#     "time_created": "2013-03-22T21:18:54+0000",
#     "time_updated": "2013-03-22T21:18:55+0000"
# },
#     {
#         "type": "refund",
#     "status": "completed",
#     "currency": "USD",
#     "amount": "0.99",
#     "time_created": "2013-03-23T21:18:54+0000",
#     "time_updated": "2013-03-23T21:18:55+0000"
# }
# ],
#     "refundable_amount": {
#     "currency": "USD",
#     "amount": "0.00"
# },
#     "items": [
#     {
#         "type": "IN_APP_PURCHASE",
#     "product": "http://www.friendsmash.com/og/friend_smash_bomb.html",
#     "quantity": 1
# }
# ],
#     "country": "US",
#     "created_time": "2013-03-22T21:18:54+0000",
#     "payout_foreign_exchange_rate": 1,
# }

###