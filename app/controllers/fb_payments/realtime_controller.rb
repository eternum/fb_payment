require_dependency "fb_payments/application_controller"

module FbPayments
  class RealtimeController < ApplicationController
    layout nil
    def challenge
      render :text =>params["hub.challenge"]
    end
    def payments_create
      File.open("TestTransaction.json", 'w') { |file| file.write(params) }
      json = JSON.parse(request.body.read)
      json["entry"].each do |entry|
        FacebookPayment.build_from_api(entry)
      end
    end
  end
end
