# coding: utf-8
require "httpclient"

class FacebookPayment

  @http = HTTPClient.new
  def self.oauth
    Koala::Facebook::OAuth.new('613226912137769',"41876506ca386a989087108b877a4b7c","http://anonwarz.cagedmedia.com/app/login/")
  end
  def self.get(id)
    response = @http.get("https://graph.facebook.com/#{id}", "access_token" =>   oauth.get_app_access_token)
    JSON.parse(response.body)
  end

  attr_reader :id

  def initialize(details)
    @id = details["id"]
    @item = details["items"].first
    @actions = details["actions"]
  end



  def og_url
    @item["product"]
  end

  def fulfilled?
    action = @actions.first
    @actions.size == 1 && action["type"] == "charge" && action["status"] == "completed"
  end

  def package
    PACKAGES.values.find { |package| package["og_url"] == og_url }
  end
end
